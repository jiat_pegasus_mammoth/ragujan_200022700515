package com.rag.db;

import com.rag.util.ApplicationProperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbConnect {
    private static Connection connection;
    private static String password = "ragJN100Mania";
    private static String url = "jdbc:mysql://localhost:3306/web_db?useSSL = false";
    private static String port = "3306";
    private static String dbName = "web_db";
    private static String username = "root";

    public static Connection getConnection() throws Exception{
        ApplicationProperties  props = ApplicationProperties.getInstance();
        if(connection == null){
            Class.forName(props.getDriver());
            DriverManager.getConnection(props.getUrl(),"root",props.getPassword());
        }

        return connection;
    }
    public static void loadProperties(){
        try {

            InputStream input = new FileInputStream("src/main/resources/application.properties");
            Properties prop = new Properties();
            prop.load(input);
            System.out.println(prop.getProperty("sql.connection.password"));
        }catch (IOException exception){
            exception.printStackTrace();
        }
    }


}
