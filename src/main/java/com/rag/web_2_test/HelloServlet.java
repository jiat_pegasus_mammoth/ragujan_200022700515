package com.rag.web_2_test;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

import com.rag.db.DbConnect;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/rag")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        Connection connection = null;
        try {
             connection = DbConnect.getConnection();
            out.println("<h1>" + message + "</h1>");
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            try {
                connection.close();
            }catch (SQLException sqlException){
                sqlException.printStackTrace();
            }
        }
        // Hello

        out.println("</body></html>");

    }

    public void destroy() {
    }
}