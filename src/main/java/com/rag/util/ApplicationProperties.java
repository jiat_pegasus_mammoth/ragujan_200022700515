package com.rag.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationProperties {

    public static String getUrl() {
        return url;
    }

    public static String getPassword() {
        return password;
    }

    public static String getDriver() {
        return driver;
    }

    private static String url;
    private static String password;
    private static String driver;
    private Properties properties;

    private static ApplicationProperties applicationProperties;

    public ApplicationProperties() {
        properties = new Properties();
        loadProperties();
    }
    public static ApplicationProperties getInstance(){
        if(applicationProperties == null){
            applicationProperties = new ApplicationProperties();
        }
        return applicationProperties;
    }
    public  void loadProperties() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
//            InputStream input = new FileInputStream("src/main/resources/application.properties");
            Properties prop = new Properties();
            prop.load(inputStream);
            url = prop.getProperty("sql.connection.url");
            password = prop.getProperty("sql.connection.password");
            driver = prop.getProperty("sql.connection.driver");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
